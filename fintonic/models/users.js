'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Modelo para los usuarios
const UserSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  registerDate: {
    type: Date,
    default:Date.now
  }
});

module.exports = mongoose.model('users', UserSchema);
