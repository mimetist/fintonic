'use strict';

const uuidv4    = require('uuid/v4');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Modelo para los productos
const UserSchema = new Schema({
  productUniqueIdentifier: {
    type: String,
    required: true,
    unique: true,
    default: () => uuidv4()
  },
  productName: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  }
});

module.exports = mongoose.model('products', UserSchema);
