'use strict';

const Product = require('../../models/products');
const logger  = require('../logger').info;

/*
 * Registramos el producto en el mongo.Primero cogemos
 * lo que nos llega por el body y en caso de que todo sea correcto se inserta
 * el nuevo registr
*/
const registerProduct = (req, res) => {
    Product.find().then((product)=> {
        const newProduct = new Product({
            productName : req.body.productName,
            description : req.body.description
        });

        newProduct.
        save().
        then((product)=> res.json(product)).
        catch(err => logger.error('Error registring new product'));
    })
}

//Para borrar un registro de un producto se envia por body el nombre del producto
const deleteProduct = (req, res)=> {
    Product.findOneAndRemove({productName: req.body.productName}).then(()=> {
        res.json({success: true});
  });
}

//Sacamos de la BBDD todos los productos
const getAllProducts = (req, res)=> {
    Product.find().then((product)=> {
        res.json(product);
    });
}

module.exports = {
    registerProduct,
    deleteProduct,
    getAllProducts
}
