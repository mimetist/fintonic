Para ejecutar correctamente el software se puede hacer de dos maneras:
    1.Tener instalado docker y docker-compose en el sistema.Para arrancarlo con docker-compose simplemente se pone el siguiente comando:::docker-compose up -d
    2.Este caso se ejecuta de manera local sin dockers.Se requiere tener instalado mongo y además en el index del archivo config de la carpeta fintonic se
    debe cambiar la url apuntando a tu mongo en local.Por defecto está puesta la del docker...

En cuanto a los endpoints para insertar y eliminar productos se debe registrar un usuario y en el endpoint /api/login se pide genera el jwt que se usara para insertar y eliminar productos.La cabecera es Authorization y el param es el valor que se obtiene
en el token de login...

Adjunto también la coleccion de Postman para este ejercicio